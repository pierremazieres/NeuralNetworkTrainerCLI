#!/usr/bin/env python3
# coding=utf-8
# import
from setuptools import setup, find_packages
# for version norm, see : https://www.python.org/dev/peps/pep-0440/#post-releases
# define setup parameters
setup(
    name="NeuralNetworkTrainerCLI",
    version="0.0.3",
    description="Neural network trainer CLI",
    packages=find_packages(),
    install_requires=["requests","termplot","neuralnetworkcommon"],
    data_files=[("shell/neuralnetworktrainercli", ["neuralnetworktrainercli/help/neuralnetworktrainercli.txt"])],
    classifiers=[
        'Programming Language :: Python :: 3',
    ],
)

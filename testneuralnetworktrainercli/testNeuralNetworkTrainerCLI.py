#!/usr/bin/env python3
# coding=utf-8
# import
from testneuralnetworkcommon.commonUtilities import CommonTest, insertRandomTrainingElements, randomTrainingSet, insertRandomTrainingSets, deleteAllByTrainingSetId, randomTrainingElement, insertRandomTrainingSessions, randomTrainingSession, randomTrainingSessionProgress, randomizeTrainingSessionElementsError, insertRandomTrainingSessionProgresses
from neuralnetworktrainercli.neuralNetworkTrainerCLI import dispatchRequest, parser, ShortCode, branchErrorMsg
from neuralnetworkcommon.service.service import ResourcePathType, TrainerResourcePathType, TrainerAction
from neuralnetworkcommon.cli.cli import ActionType, ShortCode as ShortCodeDefault
from pythoncommontools.objectUtil.POPO import loadFromDict
from json import loads
from testpythoncommontools.cli.cli import checkRestClientDefaultError
from pythoncommontools.file.file import writeTemporaryFile, readFromFile
from pythoncommontools.cli.cli import main
from random import randint, choice
from string import ascii_letters
from numpy.random import random, uniform
from neuralnetworkcommon.entity.trainingSet.trainingSet import TrainingSet
from neuralnetworkcommon.entity.trainingElement import TrainingElement
from neuralnetworkcommon.database.trainingSessionProgress import selectByPerceptronId as trainingSessionProgressSelect, deleteByPerceptronId as trainingSessionProgressDelete, insert as trainingSessionProgressInsert
from neuralnetworkcommon.database.trainingSessionElement import selectAllByPerceptronIdAndFilter
from neuralnetworkcommon.entity.trainingSession.trainingSession import TrainingSession
from neuralnetworkcommon.database.trainingElement import selectById
from pythoncommontools.service.httpSymbol import arrayToQuotedString
from neuralnetworkcommon.database.trainingSession import selectByPerceptronId as trainingSessionSelect ,insert as trainingSessionInsert,deleteByPerceptronId as trainingSessionDelete, updatePid
# test neural network CLI
badTrainingSet = TrainingSet('', -1).dumpToSimpleJson().encode()
badTrainingElement = TrainingElement('', -1, [], []).dumpToSimpleJson().encode()
badTrainingSession = TrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId,1).dumpToSimpleJson().encode()
badTrainingSessionProgress = randomTrainingSessionProgress('').dumpToSimpleJson().encode()
class testNeuralNetworkTrainerCliWS(CommonTest):
    # test CRUD OK
    def testCrudOk(self):
        # initial training set
        CommonTest.connection.commit()
        rawInitialTrainingSet = randomTrainingSet()
        tempFile = writeTemporaryFile(rawInitialTrainingSet.dumpToSimpleJson().encode())
        # create training set
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        trainingSetId = main(args, parser, dispatchRequest)
        rawInitialTrainingSet.id = trainingSetId
        self.assertIsNotNone(trainingSetId,"ERROR : trainingSet has no id")
        # read training set
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.ID.value,str(trainingSetId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedInsertedTrainingSet = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(rawInitialTrainingSet,fetchedInsertedTrainingSet,"ERROR : inserted trainingSet does not match")
        # update training set
        rawNewTrainingSet = randomTrainingSet(trainingSetId)
        tempFile = writeTemporaryFile(rawNewTrainingSet.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.ID.value,str(trainingSetId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedUpdatedTrainingSet = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedUpdatedTrainingSet,fetchedInsertedTrainingSet,"ERROR : trainingSet not updated")
        rawNewTrainingSet.id = trainingSetId
        self.assertEqual(fetchedUpdatedTrainingSet,rawNewTrainingSet,"ERROR : updated trainingSet does not match")
        # summarize training set
        trainingElementNumber = len(insertRandomTrainingElements(trainingSetId))
        CommonTest.connection.commit()
        args = '',ActionType.SUMMARIZE.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.ID.value,str(trainingSetId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        trainingSetSummary = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(trainingSetSummary.id,fetchedUpdatedTrainingSet.id,"ERROR : trainingSet summary id does not match")
        self.assertEqual(trainingSetSummary.workspaceId,fetchedUpdatedTrainingSet.workspaceId,"ERROR : trainingSet summary workspaceId does not match")
        self.assertEqual(trainingSetSummary.comments,fetchedUpdatedTrainingSet.comments,"ERROR : trainingSet summary comments does not match")
        self.assertEqual(trainingSetSummary.trainingElementsNumber,trainingElementNumber,"ERROR : trainingSet summary trainingElementsNumber does not match")
        deleteAllByTrainingSetId(CommonTest.cursor,trainingSetId)
        # patch training set
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        patchData = TrainingSet(trainingSetId,CommonTest.testWorkspaceId,updatedComments)
        tempFile = writeTemporaryFile(patchData.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.PATCH.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        CommonTest.connection.commit()
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.ID.value,str(trainingSetId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedPatchedPerceptron = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedPatchedPerceptron.comments,fetchedUpdatedTrainingSet.comments,"ERROR : patched trainingSet comments not updated")
        self.assertEqual(fetchedPatchedPerceptron.comments,updatedComments,"ERROR : patched trainingSet comments does not match")
        # initial training element
        rawInitialTrainingElement = randomTrainingElement(trainingSetId=trainingSetId)
        tempFile = writeTemporaryFile(rawInitialTrainingElement.dumpToSimpleJson().encode())
        # create training element
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        trainingElementId = main(args, parser, dispatchRequest)
        rawInitialTrainingElement.id = trainingElementId
        self.assertIsNotNone(trainingElementId,"ERROR : trainingElement has no id")
        # read training element
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.ID.value,str(trainingElementId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedInsertedTrainingElement = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(rawInitialTrainingElement,fetchedInsertedTrainingElement,"ERROR : inserted trainingElement does not match")
        # update training element
        rawNewTrainingElement = randomTrainingElement(trainingElementId,trainingSetId)
        tempFile = writeTemporaryFile(rawNewTrainingElement.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.ID.value,str(trainingElementId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedUpdatedTrainingElement = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedUpdatedTrainingElement,fetchedInsertedTrainingElement,"ERROR : trainingElement not updated")
        rawNewTrainingElement.id = trainingElementId
        self.assertEqual(fetchedUpdatedTrainingElement,rawNewTrainingElement,"ERROR : updated trainingElement does not match")
        # initial training session
        CommonTest.connection.commit()
        rawInitialTrainingSession = randomTrainingSession(CommonTest.testPerceptronId,trainingSetId,None,None,None)
        tempFile = writeTemporaryFile(rawInitialTrainingSession.dumpToSimpleJson().encode())
        randomizeTrainingSessionElementsError(CommonTest.testPerceptronId)
        testRatio = random()
        # create training session
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCode.TEST_RATIO.value,str(testRatio),ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        # INFO : nothing to check, creating session ID does not return any ID
        # read training session
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedInsertedTrainingSession = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(rawInitialTrainingSession,fetchedInsertedTrainingSession,"ERROR : inserted trainingSession does not match")
        # insert training progress
        trainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId, randint(1000, 9999))
        trainingSessionProgressInsert(CommonTest.cursor,trainingSessionProgress)
        insertedTrainingSessionProgress = trainingSessionProgressSelect(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNotNone(insertedTrainingSessionProgress, "ERROR : trainingSessionProgress not inserted")
        # update training session
        rawNewTrainingSession = randomTrainingSession(CommonTest.testPerceptronId,trainingSetId)
        tempFile = writeTemporaryFile(rawNewTrainingSession.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCode.TEST_RATIO.value,str(testRatio),ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedUpdatedTrainingSession = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedUpdatedTrainingSession,fetchedInsertedTrainingSession,"ERROR : training session not updated")
        self.assertEqual(fetchedUpdatedTrainingSession,rawNewTrainingSession,"ERROR : updated trainingSession does not match")
        updatedTrainingSessionProgress = trainingSessionProgressSelect(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNotNone(updatedTrainingSessionProgress, "ERROR : trainingSessionProgress not deleted")
        # summarize training session
        CommonTest.connection.commit()
        args = '',ActionType.SUMMARIZE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        trainingSessionSummary = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        saveInterval = fetchedUpdatedTrainingSession.saveInterval
        fetchedTrainingSessionProgress = trainingSessionProgressSelect(CommonTest.cursor,CommonTest.testPerceptronId)
        updated_allIdsByPerceptronId_fineTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=False));
        updated_allIdsByPerceptronId_errorTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=True));
        updated_allIdsByPerceptronId_fineTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=False));
        updated_allIdsByPerceptronId_errorTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=True));
        self.assertEqual(trainingSessionSummary.perceptronId,fetchedUpdatedTrainingSession.perceptronId,"ERROR : trainingSession summary perceptronId does not match")
        self.assertEqual(trainingSessionSummary.trainingSetId,fetchedUpdatedTrainingSession.trainingSetId,"ERROR : trainingSession summary trainingSetId does not match")
        self.assertEqual(trainingSessionSummary.saveInterval,saveInterval,"ERROR : trainingSession summary saveInterval does not match")
        self.assertEqual(trainingSessionSummary.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : trainingSession summary maximumTry does not match")
        self.assertEqual(trainingSessionSummary.maximumErrorRatio,fetchedUpdatedTrainingSession.maximumErrorRatio,"ERROR : trainingSession summary maximumTry does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.fineTraining,len(updated_allIdsByPerceptronId_fineTraining),"ERROR : fine trainingSession summary trainingSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.errorTraining,len(updated_allIdsByPerceptronId_errorTraining),"ERROR : error trainingSession summary testSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.fineTest,len(updated_allIdsByPerceptronId_fineTest),"ERROR : fine trainingSession summary trainingSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.errorTest,len(updated_allIdsByPerceptronId_errorTest),"ERROR : error trainingSession summary testSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.pid,fetchedUpdatedTrainingSession.pid,"ERROR : trainingSession summary pid does not match")
        self.assertEqual(trainingSessionSummary.errorMessage,fetchedUpdatedTrainingSession.errorMessage,"ERROR : trainingSession summary errorMessage does not match")
        self.assertEqual(trainingSessionSummary.progressRecordsNumber,len(fetchedTrainingSessionProgress.meanDifferentialErrors),"ERROR : trainingSession summary errorMessage does not match")
        self.assertEqual(trainingSessionSummary.meanDifferentialErrors,fetchedTrainingSessionProgress.meanDifferentialErrors[-1],"ERROR : trainingSession summary meanDifferentialError does not match")
        self.assertEqual(trainingSessionSummary.errorElementsNumbers,fetchedTrainingSessionProgress.errorElementsNumbers[-1],"ERROR : trainingSession summary errorElementsNumber does not match")
        self.assertEqual(trainingSessionSummary.testScore,fetchedUpdatedTrainingSession.testScore,"ERROR : trainingSession summary testScore does not match")
        self.assertEqual(trainingSessionSummary.comments,fetchedUpdatedTrainingSession.comments,"ERROR : trainingSession summary comments does not match")
        # patch training session
        updatedSaveInterval = int(saveInterval * uniform(2,10))
        updatedMaximumTry = randint(11,20)
        updatedTrainingMaximumErrorRatio = random()
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        patchData = TrainingSession(CommonTest.testPerceptronId,saveInterval=updatedSaveInterval,maximumTry=updatedMaximumTry,maximumErrorRatio=updatedTrainingMaximumErrorRatio,comments=updatedComments)
        tempFile = writeTemporaryFile(patchData.dumpToSimpleJson().encode(),tempFile)
        CommonTest.connection.commit()
        args = '',ActionType.PATCH.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedReupdatedTrainingSessionProgress = trainingSessionProgressSelect(CommonTest.cursor,CommonTest.testPerceptronId)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedPatchedTrainingSession = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        trainingSessionProgressDelete(CommonTest.cursor,CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        expectedShrinkRatio = updatedSaveInterval / saveInterval
        delta = .1
        self.assertNotEqual(fetchedPatchedTrainingSession.saveInterval,fetchedUpdatedTrainingSession.saveInterval,"ERROR : reupdated saveInterval not updated")
        self.assertEqual(fetchedPatchedTrainingSession.saveInterval,updatedSaveInterval,"ERROR : reupdated saveInterval does not match")
        self.assertNotEqual(fetchedPatchedTrainingSession.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : reupdated maximumTry not updated")
        self.assertEqual(fetchedPatchedTrainingSession.maximumTry,updatedMaximumTry,"ERROR : reupdated maximumTry does not match")
        self.assertNotEqual(fetchedPatchedTrainingSession.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : reupdated maximumTry not updated")
        self.assertEqual(fetchedPatchedTrainingSession.maximumTry,updatedMaximumTry,"ERROR : reupdated maximumTry does not match")
        self.assertNotEqual(fetchedPatchedTrainingSession.comments,fetchedUpdatedTrainingSession.comments,"ERROR : patched trainingSession comments not updated")
        self.assertEqual(fetchedPatchedTrainingSession.comments,updatedComments,"ERROR : patched trainingSession comments does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.meanDifferentialErrors) / len(fetchedReupdatedTrainingSessionProgress.meanDifferentialErrors), expectedShrinkRatio, delta=delta, msg="ERROR : reupdated meanDifferentialErrors ratio does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.errorElementsNumbers) / len(fetchedReupdatedTrainingSessionProgress.errorElementsNumbers), expectedShrinkRatio, delta=delta, msg="ERROR : reupdated errorElementsNumbers ratio does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.resets) / len(fetchedReupdatedTrainingSessionProgress.resets), expectedShrinkRatio, delta=delta, msg="ERROR : reupdated resets ratio does not match")
        self.assertEqual(fetchedPatchedTrainingSession.pid,fetchedUpdatedTrainingSession.pid,"ERROR : reupdated pid does not match")
        self.assertEqual(fetchedPatchedTrainingSession.errorMessage,fetchedUpdatedTrainingSession.errorMessage,"ERROR : reupdated errorMessage does not match")
        self.assertEqual(fetchedPatchedTrainingSession.testScore,fetchedUpdatedTrainingSession.testScore,"ERROR : reupdated testScore does not match")
        # initialize session progress
        rawInitiaTrainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        tempFile = writeTemporaryFile(rawInitiaTrainingSessionProgress.dumpToSimpleJson().encode())
        # create training session progress
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        # INFO : nothing to check, creating session ID does not return any ID
        # read training session progress
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedInsertedTrainingSessionProgress = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(rawInitiaTrainingSessionProgress,fetchedInsertedTrainingSessionProgress,"ERROR : inserted trainingSessionProgress does not match")
        # update training session progress
        rawNewTrainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId)
        tempFile = writeTemporaryFile(rawNewTrainingSessionProgress.dumpToSimpleJson().encode())
        # check update
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedUpdatedTrainingSessionProgress = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedUpdatedTrainingSessionProgress,fetchedInsertedTrainingSessionProgress,"ERROR : training session not updated")
        self.assertEqual(fetchedUpdatedTrainingSessionProgress,rawNewTrainingSessionProgress,"ERROR : updated trainingSessionProgress does not match")
        # start
        startArgs = '',ActionType.EXECUTE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCode.TRAINER_ACTION.value,TrainerAction.START.value
        firstPid = main(startArgs, parser, dispatchRequest)
        self.assertIsNotNone(firstPid,"ERROR : PID not set on start")
        fetchedStartedTrainingSession = trainingSessionSelect(CommonTest.cursor,CommonTest.testPerceptronId)
        sessionErrorMessage = fetchedStartedTrainingSession.errorMessage
        started = fetchedStartedTrainingSession.pid or sessionErrorMessage
        self.assertTrue(started,"ERROR : trainer did not start")
        # stop
        stopArgs = '',ActionType.EXECUTE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCode.TRAINER_ACTION.value,TrainerAction.STOP.value
        main(stopArgs, parser, dispatchRequest)
        fetchedStoppedTrainingSession = trainingSessionSelect(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedStoppedTrainingSession.pid,"ERROR : stopped with PID value")
        self.assertEqual(fetchedStoppedTrainingSession.errorMessage,sessionErrorMessage,"ERROR : stopped with new errorMessage")
        # simulate crashed trainer : a PID is register in database but there is no associated process running
        updatePid(CommonTest.cursor,CommonTest.testPerceptronId, firstPid)
        CommonTest.connection.commit()
        newPid = main(startArgs, parser, dispatchRequest)
        self.assertNotEqual(newPid,firstPid,"ERROR : crashed trainer did not restart")
        fetchedRestartedTrainingSession = trainingSessionSelect(CommonTest.cursor,CommonTest.testPerceptronId)
        sessionErrorMessage = fetchedRestartedTrainingSession.errorMessage
        self.assertEqual(sessionErrorMessage , fetchedRestartedTrainingSession.errorMessage,"ERROR : trainer did not restart")
        # stop crashed trainer
        main(stopArgs, parser, dispatchRequest)
        # delete training session progress
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId)
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        decompressedData = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertIsNone(decompressedData,"ERROR : trainingSessionProgress not deleted")
        # delete training session
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId)
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        decompressedData = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertIsNone(decompressedData,"ERROR : trainingSession not deleted")
        # delete training element
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.ID.value,str(trainingElementId)
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.ID.value,str(trainingElementId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        decompressedData = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertIsNone(decompressedData,"ERROR : trainingElement not deleted")
        # delete training set
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.ID.value,str(trainingSetId)
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.ID.value,str(trainingSetId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        decompressedData = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertIsNone(decompressedData,"ERROR : trainingSet not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize data
        trainingSetsIds=insertRandomTrainingSets()
        trainingSession = randomTrainingSession(CommonTest.testPerceptronId, CommonTest.testTrainingSetId)
        trainingElementsIds = set(insertRandomTrainingElements(trainingSetId=CommonTest.testTrainingSetId))
        trainingSessionInsert(CommonTest.cursor,trainingSession,random())
        randomizeTrainingSessionElementsError(CommonTest.testPerceptronId)
        trainingSessionPerceptronIds = insertRandomTrainingSessions()
        trainingSessionProgressPerceptronIds = insertRandomTrainingSessionProgresses()
        CommonTest.connection.commit()
        # select training sets IDs
        readTrainingSetArgs = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        fetchedIds = main(readTrainingSetArgs, parser, dispatchRequest)
        self.assertTrue(trainingSetsIds.issubset(fetchedIds),"ERROR : trainingSet IDs selection does not match")
        # select training elements IDs from training set
        readTrainingSetArgs = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testTrainingSetId), ShortCode.RELATED_BRANCH.value, TrainerResourcePathType.GLOBAL_TRAININGSET.value
        fetchedIds = main(readTrainingSetArgs, parser, dispatchRequest)
        trainingSet_expectedFirstId = fetchedIds[0]
        trainingSet_fetchedIds = set(fetchedIds)
        firstElement = selectById(CommonTest.cursor,trainingSet_expectedFirstId)
        expectedOutput = arrayToQuotedString(firstElement.expectedOutput)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testTrainingSetId), ShortCode.RELATED_BRANCH.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCode.EXPECTED_OUTPUT.value,expectedOutput
        trainingSet_fetchedIds_ExpectedOutput = set(main(args, parser, dispatchRequest))
        self.assertTrue(trainingElementsIds.issubset(trainingSet_fetchedIds),"ERROR : IDs selection does not match")
        self.assertSetEqual(trainingSet_fetchedIds_ExpectedOutput, {trainingSet_expectedFirstId},msg="ERROR : trainingSet first ID does not match")
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.TEST_FILTER.value,"false"
        perceptron_fetchedIds_training = set(main(args, parser, dispatchRequest))
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.TEST_FILTER.value,"true"
        perceptron_fetchedIds_test = set(main(args, parser, dispatchRequest))
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.ERROR_FILTER.value,"false"
        perceptron_fetchedIds_fine = set(main(args, parser, dispatchRequest))
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.ERROR_FILTER.value,"true"
        perceptron_fetchedIds_error = set(main(args, parser, dispatchRequest))
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.TEST_FILTER.value,"false",ShortCode.ERROR_FILTER.value,"false"
        perceptron_fetchedIds_trainingFine = set(main(args, parser, dispatchRequest))
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.TEST_FILTER.value,"false",ShortCode.ERROR_FILTER.value,"true"
        perceptron_fetchedIds_trainingError = set(main(args, parser, dispatchRequest))
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.TEST_FILTER.value,"true",ShortCode.ERROR_FILTER.value,"false"
        perceptron_fetchedIds_testFine = set(main(args, parser, dispatchRequest))
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.TEST_FILTER.value,"true",ShortCode.ERROR_FILTER.value,"true"
        perceptron_fetchedIds_testError = set(main(args, parser, dispatchRequest))
        firstElement = selectById(CommonTest.cursor,trainingSet_expectedFirstId)
        expectedOutput = arrayToQuotedString(firstElement.expectedOutput)
        args = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId), ShortCode.RELATED_BRANCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCode.EXPECTED_OUTPUT.value,expectedOutput
        perceptron_fetchedIds_ExpectedOutput = set(main(args, parser, dispatchRequest))
        self.assertSetEqual(perceptron_fetchedIds_trainingFine.union(perceptron_fetchedIds_trainingError), perceptron_fetchedIds_training,msg="ERROR : training IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_testFine.union(perceptron_fetchedIds_testError), perceptron_fetchedIds_test,msg="ERROR : test IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_trainingFine.union(perceptron_fetchedIds_testFine), perceptron_fetchedIds_fine,msg="ERROR : fine IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_trainingError.union(perceptron_fetchedIds_testError), perceptron_fetchedIds_error,msg="ERROR : error IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_training.union(perceptron_fetchedIds_test), trainingSet_fetchedIds,msg="ERROR : training + test IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_fine.union(perceptron_fetchedIds_error), trainingSet_fetchedIds,msg="ERROR : error + fine IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_ExpectedOutput, {trainingSet_expectedFirstId},msg="ERROR : perceptron first ID does not match")
        self.assertEqual(trainingSet_fetchedIds,trainingSet_fetchedIds,"ERROR : elements IDs does not match")
        # select training sessions IDs
        readTrainingSessionArgs = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        fetchedIds = main(readTrainingSessionArgs, parser, dispatchRequest)
        self.assertTrue(trainingSessionPerceptronIds.issubset(fetchedIds),"ERROR : trainingSession IDs selection does not match")
        # select training session progresses IDs
        readTrainingSessionProgressArgs = '',ActionType.READ.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        fetchedIds = main(readTrainingSessionProgressArgs, parser, dispatchRequest)
        self.assertTrue(trainingSessionProgressPerceptronIds.issubset(fetchedIds),"ERROR : trainingSessionProgress IDs selection does not match")
        # delete all training sessions progresses
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        main(args, parser, dispatchRequest)
        remainingIds = main(readTrainingSessionProgressArgs, parser, dispatchRequest)
        self.assertEqual(len(remainingIds),0,"ERROR : trainingSessionProgress IDs deletion does not match")
        # delete all training sessions
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        main(args, parser, dispatchRequest)
        remainingIds = main(readTrainingSessionArgs, parser, dispatchRequest)
        self.assertEqual(len(remainingIds),0,"ERROR : trainingSession IDs deletion does not match")
        # delete all training elements
        trainingSessionDelete(CommonTest.cursor,CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testTrainingSetId)
        main(args, parser, dispatchRequest)
        remainingIds = main(readTrainingSetArgs, parser, dispatchRequest)
        self.assertEqual(len(remainingIds),0,"ERROR : trainingSet IDs deletion does not match")
        # delete all training sets
        args = '',ActionType.DELETE.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        main(args, parser, dispatchRequest)
        remainingIds = main(readTrainingSetArgs, parser, dispatchRequest)
        self.assertEqual(len(remainingIds),0,"ERROR : trainingSet IDs deletion does not match")
        pass
    # test error
    def testPostTrainingSetError(self):
        badTempFile = writeTemporaryFile(badTrainingSet)
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPutTrainingSetError(self):
        badTempFile = writeTemporaryFile(badTrainingSet)
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPatchTrainingSetError(self):
        badTempFile = writeTemporaryFile(badTrainingSet)
        args = '',ActionType.PATCH.value,TrainerResourcePathType.GLOBAL_TRAININGSET.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPostTrainingElementError(self):
        badTempFile = writeTemporaryFile(badTrainingElement)
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPutTrainingElementError(self):
        badTempFile = writeTemporaryFile(badTrainingElement)
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPostTrainingSessionError(self):
        badTempFile = writeTemporaryFile(badTrainingSession)
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPutTrainingSessionError(self):
        badTempFile = writeTemporaryFile(badTrainingSession)
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPatchTrainingSessionError(self):
        badTempFile = writeTemporaryFile(badTrainingSession)
        args = '',ActionType.PATCH.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPostTrainingSessionProgressError(self):
        badTempFile = writeTemporaryFile(badTrainingSessionProgress)
        args = '',ActionType.CREATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPutTrainingSessionProgressError(self):
        badTempFile = writeTemporaryFile(badTrainingSessionProgress)
        args = '',ActionType.UPDATE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testNoTrainerAction(self):
        args = '',ActionType.EXECUTE.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId)
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
    pass
pass

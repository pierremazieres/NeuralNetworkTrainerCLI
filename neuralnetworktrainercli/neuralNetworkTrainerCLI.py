#!/usr/bin/env python3
# coding=utf-8
# import
from enum import Enum, unique
from neuralnetworkcommon.cli.cli import parser, ActionType as CommonActionType, create, readAll, readSingle, update, delete, patch, summarize
from neuralnetworkcommon.service.service import ResourcePathType, TrainerResourcePathType, TrainerAction, globalTrainingSetUrl, globalTrainingElementUrl, globalTrainingSessionUrl, globalTrainingSessionProgressUrl, specificTrainingSetUrl, trainingSetSummaryUrl, specificTrainingElementUrl, specificTrainingSessionUrl, trainingSessionSummaryUrl, specificTrainingSessionProgressUrl, executeTrainerUrl
from os import sep, environ
from os.path import join, realpath
from sys import argv
from pythoncommontools.service.httpSymbol import getServerUrl
from pythoncommontools.cli.cli import main, raiseArgumentsException
'''
from gzip import decompress
from json import loads
from os import popen
from termplot import plot
'''
# contants
currentDirectory = realpath(__file__).rsplit(sep, 1)[0]
extraHelpFile=join(currentDirectory,"help","neuralnetworktrainercli.txt")
branchErrorMsg = "ERROR : branch does not match"
# load configuration
host = environ.get("NEURALNETWORK_TRAINER_CLI_HOST")
port = int(environ.get("NEURALNETWORK_TRAINER_SERVICE_PORT"))
endpoint = environ.get("NEURALNETWORK_TRAINER_SERVICE_ENDPOINT")
serverUrl=getServerUrl(host,port)
# global URL
serverGlobalTrainingSetUrl = serverUrl+globalTrainingSetUrl(endpoint)
serverGlobalTrainingElementUrl = serverUrl+globalTrainingElementUrl(endpoint)
serverGlobalTrainingSessionUrl = serverUrl+globalTrainingSessionUrl(endpoint)
serverGlobalTrainingSessionProgressUrl = serverUrl+globalTrainingSessionProgressUrl(endpoint)
# CLI parameter
@unique
class ShortCode(Enum):
    TRAINER_ACTION="-a"
    RELATED_BRANCH="-b"
    ERROR_FILTER="-e"
    EXPECTED_OUTPUT="-o"
    TEST_RATIO="-R"
    TEST_FILTER="-T"
''' TODO : set default value for below parameters (and review proposed default values) :
 - training-maximum-error-ratio = 1% => on server side, post method
 - test-ratio = 1/10
 - maximum-try = len(traning-set) => on server side, post method
 - save-interval = len(traning-set) => on server side, post method
'''
parser.add_argument("resource", type=str, choices=((TrainerResourcePathType.GLOBAL_TRAININGSET.value,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value)), help="resource to use")
parser.add_argument(ShortCode.TRAINER_ACTION.value, "--trainer-action", type=str, choices=((TrainerAction.START.value,TrainerAction.STOP.value)),help="action")
parser.add_argument(ShortCode.RELATED_BRANCH.value, "--related-branch", type=str, choices=((TrainerResourcePathType.GLOBAL_TRAININGSET.value,ResourcePathType.GLOBAL_PERCEPTRON.value)),help="branch (related orientation)")
parser.add_argument(ShortCode.ERROR_FILTER.value, "--error-filter", type=str,help="filter training session results on error, ie. true or false")
parser.add_argument(ShortCode.EXPECTED_OUTPUT.value, "--expected-output", type=str,help="expected output vector (comma ',' separated values), ex. 0.19,0.73,...")
parser.add_argument(ShortCode.TEST_RATIO.value, "--test-ratio", type=float,help="ratio (between 0 and 1) of training set element that will be used for testing (after training), ex. 0.25")
parser.add_argument(ShortCode.TEST_FILTER.value, "--test-filter", type=str,help="filter training session results on test (opposite to training), ie. true or false")
# ***** TRAINING SET *****
# create
def createTrainingSet(sourceFile):
    response = create(serverGlobalTrainingSetUrl,sourceFile)
    return response
# read single
def readSingleTrainingSet(id,targetFile):
    URL=serverUrl+specificTrainingSetUrl(endpoint,id)
    readSingle(URL, targetFile)
    pass
# read all
def readAllTrainingSet(parentId):
    URL=serverUrl+globalTrainingSetUrl(endpoint,parentId)
    response = readAll(URL)
    return response
# update
def updateTrainingSet(sourceFile):
    update(serverGlobalTrainingSetUrl, sourceFile)
    pass
# patch
def patchTrainingSet(sourceFile):
    patch(serverGlobalTrainingSetUrl,sourceFile)
    pass
# summarize
def summarizeTrainingSet(id,targetFile):
    URL=serverUrl+trainingSetSummaryUrl(endpoint,id)
    summarize(URL,targetFile)
    pass
# delete single
def deleteSingleTrainingSet(id):
    URL=serverUrl+specificTrainingSetUrl(endpoint,id)
    delete(URL)
    pass
# delete all
def deleteAllTrainingSet(parentId):
    URL=serverUrl+globalTrainingSetUrl(endpoint,parentId)
    delete(URL)
    pass
# ***** TRAINING ELEMENT *****
# create
def createTrainingElement(sourceFile):
    response = create(serverGlobalTrainingElementUrl,sourceFile)
    return response
# read single
def readSingleTrainingElement(id,targetFile):
    URL=serverUrl+specificTrainingElementUrl(endpoint,id)
    readSingle(URL, targetFile)
    pass
# read all
def readAllTrainingElement(parentId,relatedBranch,expectedOutput,testFilter,errorFilter):
    trainingSetId = None
    perceptronId = None
    if relatedBranch == TrainerResourcePathType.GLOBAL_TRAININGSET.value:
        trainingSetId = parentId
    elif relatedBranch == ResourcePathType.GLOBAL_PERCEPTRON.value:
        perceptronId = parentId
    else:
        raise Exception(branchErrorMsg)
    URL=serverUrl+globalTrainingElementUrl(endpoint,trainingSetId,perceptronId,expectedOutput,testFilter,errorFilter)
    response = readAll(URL)
    return response
# update
def updateTrainingElement(sourceFile):
    update(serverGlobalTrainingElementUrl, sourceFile)
    pass
# delete single
def deleteSingleTrainingElement(id):
    URL=serverUrl+specificTrainingElementUrl(endpoint,id)
    delete(URL)
    pass
# delete all
def deleteAllTrainingElement(parentId):
    URL=serverUrl+globalTrainingElementUrl(endpoint,parentId)
    delete(URL)
    pass
# ***** TRAINING SESSION *****
# create
def createTrainingSession(sourceFile,testRatio):
    URL=serverUrl + globalTrainingSessionUrl(endpoint,testRatio=testRatio)
    create(URL,sourceFile, False)
    pass
# read single
def readSingleTrainingSession(id,targetFile):
    URL=serverUrl+specificTrainingSessionUrl(endpoint,id)
    readSingle(URL, targetFile)
    pass
# read all
def readAllTrainingSession(parentId):
    URL=serverUrl+globalTrainingSessionUrl(endpoint,parentId)
    response = readAll(URL)
    return response
# update
def updateTrainingSession(sourceFile,testRatio):
    URL=serverUrl + globalTrainingSessionUrl(endpoint,testRatio=testRatio)
    update(URL, sourceFile)
    pass
# patch
def patchTrainingSession(sourceFile):
    patch(serverGlobalTrainingSessionUrl,sourceFile)
    pass
# summarize
def summarizeTrainingSession(id,targetFile):
    URL=serverUrl+trainingSessionSummaryUrl(endpoint,id)
    summarize(URL,targetFile)
    pass
# delete single
def deleteSingleTrainingSession(id):
    URL=serverUrl+specificTrainingSessionUrl(endpoint,id)
    delete(URL)
    pass
# delete all
def deleteAllTrainingSession(parentId):
    URL=serverUrl+globalTrainingSessionUrl(endpoint,parentId)
    delete(URL)
    pass
# ***** TRAINING SESSION PROGRESS *****
# create
def createTrainingSessionProgress(sourceFile):
    create(serverGlobalTrainingSessionProgressUrl,sourceFile, False)
    pass
# read single
def readSingleTrainingSessionProgress(id,targetFile):
    URL=serverUrl+specificTrainingSessionProgressUrl(endpoint,id)
    readSingle(URL, targetFile)
    pass
# read all
def readAllTrainingSessionProgress(parentId):
    URL=serverUrl+globalTrainingSessionProgressUrl(endpoint,parentId)
    response = readAll(URL)
    return response
# update
def updateTrainingSessionProgress(sourceFile):
    update(serverGlobalTrainingSessionProgressUrl, sourceFile)
    pass
# delete single
def deleteSingleTrainingSessionProgress(id):
    URL=serverUrl+specificTrainingSessionProgressUrl(endpoint,id)
    delete(URL)
    pass
# delete all
def deleteAllTrainingSessionProgress(parentId):
    URL=serverUrl+globalTrainingSessionProgressUrl(endpoint,parentId)
    delete(URL)
    pass
# ***** TRAINER *****
# execute
def executeTrainer(id,action):
    URL=serverUrl+executeTrainerUrl(endpoint,id,action)
    sendResponse = action==TrainerAction.START.value
    response = readAll(URL,sendResponse)
    return response
# parse arguments
def dispatchRequest(arguments):
    response = None
    action = arguments.action.lower()
    resource = arguments.resource.lower()
    # ***** TRAINING SET *****
    # create
    if action==CommonActionType.CREATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value:
        response = createTrainingSet(arguments.source_file)
    # read single
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value and arguments.id:
        readSingleTrainingSet(arguments.id,arguments.target_file)
    # read all
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value:
        response = readAllTrainingSet(arguments.related_id)
    # update
    elif action==CommonActionType.UPDATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value:
        updateTrainingSet(arguments.source_file)
    # patch
    elif action==CommonActionType.PATCH.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value:
        patchTrainingSet(arguments.source_file)
    # summarize
    elif action==CommonActionType.SUMMARIZE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value:
        summarizeTrainingSet(arguments.id,arguments.target_file)
    # delete single
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value and arguments.id:
        deleteSingleTrainingSet(arguments.id)
    # delete all
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSET.value:
        deleteAllTrainingSet(arguments.related_id)
    # ***** TRAINING ELEMENT *****
    # create
    elif action==CommonActionType.CREATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value:
        response = createTrainingElement(arguments.source_file)
    # read single
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value and arguments.id:
        readSingleTrainingElement(arguments.id,arguments.target_file)
    # read all
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value:
        response = readAllTrainingElement(arguments.related_id,arguments.related_branch,arguments.expected_output,arguments.test_filter,arguments.error_filter)
    # update
    elif action==CommonActionType.UPDATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value:
        updateTrainingElement(arguments.source_file)
    # delete single
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value and arguments.id:
        deleteSingleTrainingElement(arguments.id)
    # delete all
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value:
        deleteAllTrainingElement(arguments.related_id)
    # ***** TRAINING SESSION *****
    # create
    elif action==CommonActionType.CREATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value:
        createTrainingSession(arguments.source_file,arguments.test_ratio)
    # read single
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value and arguments.id:
        readSingleTrainingSession(arguments.id,arguments.target_file)
    # read all
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value:
        response = readAllTrainingSession(arguments.related_id)
    # update
    elif action==CommonActionType.UPDATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value:
        updateTrainingSession(arguments.source_file,arguments.test_ratio)
    # patch
    elif action==CommonActionType.PATCH.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value:
        patchTrainingSession(arguments.source_file)
    # summarize
    elif action==CommonActionType.SUMMARIZE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value:
        summarizeTrainingSession(arguments.id,arguments.target_file)
    # delete single
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value and arguments.id:
        deleteSingleTrainingSession(arguments.id)
    # delete all
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value:
        deleteAllTrainingSession(arguments.related_id)
    # ***** TRAINING SESSION PROGRESS *****
    # create
    elif action==CommonActionType.CREATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value:
        createTrainingSessionProgress(arguments.source_file)
    # read single
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value and arguments.id:
        readSingleTrainingSessionProgress(arguments.id,arguments.target_file)
    # read all
    elif action==CommonActionType.READ.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value:
        response = readAllTrainingSessionProgress(arguments.related_id)
    # update
    elif action==CommonActionType.UPDATE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value:
        updateTrainingSessionProgress(arguments.source_file)
    # delete single
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value and arguments.id:
        deleteSingleTrainingSessionProgress(arguments.id)
    # delete all
    elif action==CommonActionType.DELETE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value:
        deleteAllTrainingSessionProgress(arguments.related_id)
    # ***** TRAINER *****
    # execute
    elif action==CommonActionType.EXECUTE.value and resource==TrainerResourcePathType.GLOBAL_TRAININGSESSION.value:
        response = executeTrainer(arguments.id,arguments.trainer_action)
    # ***** error in command line positional arguments *****
    else:
        raiseArgumentsException()
    return response
# run program
def drawTrainingEvolution(id,resource):
    # get full training session
    '''URL=serverUrl+HttpSymbol.PATH_SEPARATOR.value+TrainerResourcePathType.TRAININGSESSION_PROGRESS.value.replace(ResourceValueType.PERCEPTRON.value,str(id))
    response = get(URL)
    checkresponse(response)
    trainingSessionProgress = loads(decompress(response.content).decode())
    if trainingSessionProgress and resource in trainingSessionProgress and len(trainingSessionProgress[resource])>0:
        progressData = trainingSessionProgress[resource]
        # INFO : shift minimum to 0 to optimize drawing
        maximumProgressData = max(progressData)
        minimumProgressData = min(progressData)
        # INFO : do not display graph if data are always the same
        progressData = [_-minimumProgressData for _ in progressData]
        resetsData = [1 if _ else 0 for _ in trainingSessionProgress["resets"]]
        # get window dimensions
        consoleDimensions = popen("stty size", 'r').read().split()
        if len(consoleDimensions) >0:
            rows, columns = consoleDimensions
            rows = int(rows)
            columns = int(columns)
            # dispatch data on rows
            columnRatio = len(progressData) / columns
            progressDisplay = list()
            resetsDisplay = list()
            lastDataIndex = 0
            for currentDisplayIndex in range(0, columns):
                currentDataIndex = int(currentDisplayIndex * columnRatio)
                progressDisplay.append(progressData[currentDataIndex])
                # INFO : to avoid missing reset, we check all the current chunk
                resetChunks = resetsData[lastDataIndex:currentDataIndex+1]
                resetValue = 1 if 1 in resetChunks else 0
                resetsDisplay.append(resetValue)
                lastDataIndex = currentDataIndex
            # display data
            if min(progressDisplay)!=max(progressDisplay):
                # INFO : to run on all terminals, plot with only pure ASCII characters (0-127)
                plot(progressDisplay, plot_height=int(rows*3/4), plot_char='+')
                if 1 in resetsDisplay:
                    plot(resetsDisplay, plot_height=1, plot_char='^')
                else:
                    print("no reset (or not enought) to display")
                print("X scale : " + str(columnRatio) + " | X max : " + str(len(progressData)) + " | Y scale : " + str(maximumProgressData / rows) + " | Y min : " + str(minimumProgressData) + " | Y max : " + str(maximumProgressData))
            else:
                print("data is (almost) constant to : " + str(maximumProgressData))
        else:
            print("WARNING : unabale to get console dimensions")
    else:
        print("the requested resource to draw is not calculated (yet ?)")'''
    pass
if __name__ == "__main__":
    # initialize parser
    response = main(argv, parser, dispatchRequest, extraHelpFile)
    if response:
        print(response)
